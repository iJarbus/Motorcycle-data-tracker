
#include <Wire.h>
#include "RTClib.h"
#include <SPI.h>
#include <SD.h>

const int chipSelect = 4; //CS on pin 4
const int buttonPin = 2;
const int ledRed = 5;
const int ledGreen = 6;
const int ledBlue = 7;
RTC_DS1307 RTC;

// Variables will change:
bool recording = true;
int buttonState;             // the current reading from the input pin
int lastButtonState = LOW;   // the previous reading from the input pin

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an int.
unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
unsigned long timeOfLastRead = 0;
unsigned long waitBetweenReads = 1000;

void setup () {
    Serial.begin(9600);
    Wire.begin();
    if (setupRTC()) {
      Serial.println("Initialised RTC");
    }
    else {
      Serial.println("Failed to initialise RTC");
    }
    
    if (setupSD()) {
      Serial.println("Initialised SD card");
    }
    else {
      Serial.println("Failed to initialise SD card");
    }

    setupLED();
    printDevider();
    
}
void loop () {
    updateWriteButton();
    updateLED();
    if (recording) {
      if (shouldGetData()) {
        getData();
      }
    }
}

bool setupRTC() {
  RTC.begin();
  if (! RTC.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }
  return true;
}

bool setupSD() {
  while(!Serial) {
    ;
  }

  Serial.print("Initialising SD card....");

  //Check if we have a connection with the card
  if (!SD.begin(chipSelect)) {
    return false;
  }
  File dataFile = SD.open("datalog.txt", FILE_WRITE);

  if (dataFile) {
    dataFile.println("Start of new batch");
    dataFile.close();
  }
  return true;
  
}

void setupButton() {
  // Variables will change:
  bool recording = true;
  int buttonState;             // the current reading from the input pin
  int lastButtonState = LOW;   // the previous reading from the input pin

  // the following variables are unsigned longs because the time, measured in
  // milliseconds, will quickly become a bigger number than can be stored in an int.
  unsigned long lastDebounceTime = 0;  // the last time the output pin was toggled
  unsigned long debounceDelay = 50;    // the debounce time; increase if the output flickers
}

void setupLED() {
  pinMode(ledRed, OUTPUT);
  pinMode(ledGreen, OUTPUT);
  pinMode(ledBlue, OUTPUT);
}

String getTimeString() {
  DateTime now = RTC.now();
  String returnString = "";
  returnString += now.year();
  returnString += '/';
  returnString += now.month();
  returnString += '/';
  returnString += now.day();
  returnString += ' ';
  returnString += now.hour();
  returnString += ':';
  returnString += now.minute();
  returnString += ':';
  returnString += now.second();

  return returnString;
}

double getTemp() {
  analogRead(0);
  delay(10);
  int temp = analogRead(0);
  double data = (double) temp * (5/10.24);
  return data;
}

int getLight() {
  analogRead(1);
  delay(10);
  return analogRead(1);
}

void printTime() {
    Serial.println(getTimeString()); 
}

void printTemp() {
  Serial.print("Temp: ");
  Serial.print(getTemp(), DEC);
  Serial.println();
}

void printLight() {

  Serial.print("light: ");
  Serial.print(getLight(), DEC);
  Serial.println();
}

void printDevider() {
  Serial.println();
  Serial.print("=======================================================");
  Serial.println();
}

String getDataString() {
  String dataString = "";
  dataString += getTimeString();
  dataString += ",";
  dataString += getTemp();
  dataString += ",";
  dataString += getLight();

  return dataString;
}

bool writeDataString() {
  File dataFile = SD.open("datalog.txt", FILE_WRITE);

  if (dataFile) {
    dataFile.println(getDataString());
    dataFile.close();
    return true;
  }
  else {
    return false;
  }
}

void updateWriteButton() {
  // read the state of the switch into a local variable:
  int reading = digitalRead(buttonPin);

  // check to see if you just pressed the button
  // (i.e. the input went from LOW to HIGH), and you've waited long enough
  // since the last press to ignore any noise:

  // If the switch changed, due to noise or pressing:
  if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }

  if ((millis() - lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer than the debounce
    // delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != buttonState) {
      buttonState = reading;

      // only toggle the LED if the new button state is HIGH
      if (buttonState == HIGH) {
        recording = !recording;
      }
    }
  }
  
  // save the reading. Next time through the loop, it'll be the lastButtonState:
  lastButtonState = reading;
}

void updateLED() {
  if (recording) {
    analogWrite(ledRed,0);
    analogWrite(ledGreen,255);
    analogWrite(ledBlue,0);
  }
  else {
    analogWrite(ledRed,255);
    analogWrite(ledGreen,0);
    analogWrite(ledBlue,0);
  }
}

bool shouldGetData() {
  return millis() - timeOfLastRead >= 1000;
}

void getData() {
    timeOfLastRead = millis();
    printTime();
    printTemp();
    printLight();
    if (writeDataString()) {
      Serial.println("Wrote data to SD card");
    }
    else {
      Serial.println("Failed to write data to SD card");
    }
    printDevider();
}

